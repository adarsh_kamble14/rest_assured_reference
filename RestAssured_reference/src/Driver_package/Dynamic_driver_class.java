package Driver_package;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import utility_common_method.Excel_data_extractor;

public class Dynamic_driver_class {

	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<String> TC_execute=Excel_data_extractor .Excel_data_reader("test_data","Test_cases","TC_Name");
		System.out.println(TC_execute);
		int count=TC_execute.size();
		for(int i=1; i<count;i++) {
			String TC_name=TC_execute.get(i);
			System.out.println(TC_name);
			
			// call the testcaseclass on runtime by using java.lang.reflect package
			Class<?> Test_class=Class.forName("Test_package."+TC_name);
			// call the execute method belonging to test class captured in variable testclassname by using java.lang.reflect.method class
			Method execute_method=Test_class.getDeclaredMethod("executor");
			
			// set the accessibility of method true
			execute_method.setAccessible(true);
			
			// create the instance of testclass captured in variable name testclassname
			Object instance_of_test_class=Test_class.getDeclaredConstructor().newInstance();
			
			//Execute the test script class feteched in variable Test_class
			execute_method.invoke(instance_of_test_class);
			
		}
	}

}
