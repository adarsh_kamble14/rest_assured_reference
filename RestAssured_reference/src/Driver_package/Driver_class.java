package Driver_package;

import java.io.IOException;

import Test_package.patch_tc1;
import Test_package.post_TC1;
import Test_package.put_tc1;

public class Driver_class {

	public static void main(String[] args) throws IOException {

		post_TC1.executor();
		put_tc1.executor();
		patch_tc1.executor();

	}
}
