package Operators;

public class All_operators {

	public static void main(String[] args) {
		

		//Arithmetic operators
		
				int a = 20 ;
				int b = 5 ;
				
				System.out.println(a+b);
				System.out.println(b-a);
				System.out.println(a*b);
				System.out.println(a/b);
				System.out.println(a%b);

				
				//Relational operators
				
				System.out.println(a<b);
				System.out.println(a>b);
				System.out.println(a<=b);
				System.out.println(a>=b);
				System.out.println(a==b);
				System.out.println(a!=b);
				
				//logical operators
				
				boolean x=true;
				boolean y=false;
				
				System.out.println(x && y);
				System.out.println(x || y);
				System.out.println(!x);
				System.out.println(!y);
				
				//Assignment operators
				int c;
				c=a;
				System.out.println(c);
				c=b;
				System.out.println(c);
				
				c=50;
				c=c+1;
				System.out.println(c);
				
				c=c-1;
				System.out.println(c);
				
				//Increment operator
				c++; 
				System.out.println(c);
				c+=8;
				System.out.println(c);
				
				//Decrement order
				c--;
				System.out.println(c);
				c-=8;
				System.out.println(c);
				
				
				
			}

		}

