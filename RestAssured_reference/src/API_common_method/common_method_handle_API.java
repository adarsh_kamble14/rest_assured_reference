package API_common_method;

import static io.restassured.RestAssured.given;

public class common_method_handle_API {

	//POST
	public static int post_statusCode(String requestbody , String endpoint) {
	
	int statusCode = given().header("Content-Type", "application/json")
	.body(requestbody)
	.when().post(endpoint).then().extract().statusCode();
	return statusCode;
	
	}
	
public static String post_responseBody(String requestbody , String endpoint) {
	
	String responseBody = given().header("Content-Type", "application/json")
	.body(requestbody)
	.when().post(endpoint).then().extract().response().asString();
	
	return responseBody;
	
	
  }


      //put
public static int put_statusCode(String requestbody , String endpoint) {
	
	int statusCode = given().header("Content-Type", "application/json")
	.body(requestbody)
	.when().put(endpoint).then().extract().statusCode();
	return statusCode;
	
}

public static String put_responseBody(String requestbody , String endpoint) {
	
	String responseBody = given().header("Content-Type", "application/json")
	.body(requestbody)
	.when().put(endpoint).then().extract().response().asString();
	
	return responseBody;
	
}



       //patch
public static int patch_statusCode(String requestbody , String endpoint) {
	
	int statusCode = given().header("Content-Type", "application/json")
	.body(requestbody)
	.when().patch(endpoint).then().extract().statusCode();
	return statusCode;
	
}

public static String patch_responseBody(String requestbody , String endpoint) {
	
	String responseBody = given().header("Content-Type", "application/json")
	.body(requestbody)
	.when().patch(endpoint).then().extract().response().asString();
	
	return responseBody;
	
}

}



