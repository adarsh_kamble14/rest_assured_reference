package test_pkg;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;

public class post_test {

	public static void main(String[] args) {
		//declare the base url
		RestAssured.baseURI="https://reqres.in/";
		
		//configure the API
		String requestBody ="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}";
		
		String responseBody= given().header("Content-Type", "application/json").body("requestBody")
				
				.when()
				.post("api/users")
				
				.then()
				.extract().asString();
		
		//responsebody
		JsonPath jsp =new JsonPath(responseBody);
		String res_name = jsp.getString("morpheus");
		String res_job = jsp.getString("job");
		
		
				
				//validate response
				Assert.assertEquals(res_name, "morpheus");
		        Assert.assertEquals(res_job, "job");
	}

	

}
