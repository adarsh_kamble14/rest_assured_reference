import io.restassured.RestAssured;

import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;
public class patch_reference {

	public static void main(String[] args) {
		
		//step 1 declare base url
		RestAssured.baseURI="https://reqres.in/";
		
		//step 2 configure the api and trigger
		String requestbody="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		
		String responsebody=given()
		.header("Content-Type","application/json")
		.body(requestbody)
		
		.when()
		.patch("api/users/2")
		
		.then()
		.extract().response().asString();
		
		

		//step 3 create a path of json path and parse the requestbody and response body
		JsonPath jsp_req= new JsonPath(requestbody);
		String req_name =jsp_req.getString("name");
		String req_job=jsp_req.getString("job");
		LocalDateTime currentdate=LocalDateTime.now();
		String expecteddate=currentdate.toString().substring(0,11);
		
		JsonPath jsp_res= new JsonPath(responsebody);
		String res_name= jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedate=jsp_res.getString("updatedAt");
		res_updatedate= res_updatedate.substring(0,11);
		
		
		
		//validate the body
		Assert.assertEquals(res_name, "req_name");
		Assert.assertEquals(res_job, "req_job");
		Assert.assertEquals(res_updatedate, expecteddate);
		//Assert.assertEquals(res_updatedAt,"updatedAt");
		
		
		//System.out.println("validate name and job sucessfully");
	

	}

}
