import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
public class soap_rest_reference {

	public static void main(String[] args) {
		
		//declare the base URI
		RestAssured.baseURI="https://www.dataaccess.com";
		
		//declare the request body
		String requestBody="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "   <soapenv:Header/>\r\n"
				+ "   <soapenv:Body>\r\n"
				+ "      <web:NumberToWords>\r\n"
				+ "         <web:ubiNum>500</web:ubiNum>\r\n"
				+ "      </web:NumberToWords>\r\n"
				+ "   </soapenv:Body>\r\n"
				+ "</soapenv:Envelope>";
		
		//trigger the api and fetch the responsebody
		
		String responseBody=given().header("Content-Type","text/xml;charset=utf-8").body(requestBody).when().post("https://www.dataaccess.com/webservicesserver/NumberConversion.wso")
		.then().extract().response().getBody().asString();
		
		//system 
		System.out.println(responseBody);
		
		//extract the response body
		XmlPath Xml_res=new XmlPath(responseBody);
	      String res_tag=Xml_res.getString("NumberToWordsResult");
	      System.out.println(res_tag);
	      
	      //validate the responsebody
	      Assert.assertEquals(res_tag,"five hundred " );
	}

}
