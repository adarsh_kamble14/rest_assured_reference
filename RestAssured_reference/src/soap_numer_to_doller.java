import io.restassured.RestAssured;

import io.restassured.path.xml.XmlPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
public class soap_numer_to_doller {

	public static void main(String[] args) {
		//declare the base URI
		RestAssured.baseURI="https://www.dataaccess.com";
		
		//step2 
		String requestbody ="<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:web=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "   <soap:Header/>\r\n"
				+ "   <soap:Body>\r\n"
				+ "      <web:NumberToDollars>\r\n"
				+ "         <web:dNum>500</web:dNum>\r\n"
				+ "      </web:NumberToDollars>\r\n"
				+ "   </soap:Body>\r\n"
				+ "</soap:Envelope>";
		
		//validte
		String responsebody=given().header("Content-Type","text/xml;charset=utf-8").body(requestbody)
				           .when().post("webservicesserver/NumberConversion.wso")
		                   .then().extract().response().getBody().asString();
		
		System.out.println(responsebody);
		
		//extract the response body
		XmlPath xml_res=new XmlPath(responsebody);
	      String res_tag=xml_res.getString("NumberToDollarsResult");
	      System.out.println(res_tag);
	      
	      //validate
	      Assert.assertEquals(res_tag,"five hundred dollars");
		
		

	}

}
