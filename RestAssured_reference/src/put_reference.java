import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;
public class put_reference {

	public static void main(String[] args) {
		
		//Declare base URL
		RestAssured.baseURI="https://reqres.in/";
		
		//Trigger the API
		String responsebody=given().header("Content-Type","application/json").body("{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}").log().all().when().put("api/users/2").then().log().all().extract().response().asString();
		       //System.out.println("responsebody is : "+responsebody);
		
		String requestbody;
		// step 3 create a object of json path to parse the requestbody and then responsebody
		JsonPath jsp= new JsonPath (responsebody);
	            String res_name=jsp.getString("name");
	             String res_job=jsp.getString("job");
	             
	             //validaton
	             Assert.assertEquals(res_name,"morpheus");
	             Assert.assertEquals(res_job, "zion resident");
	             
	             System.out.println("validate name and job sucessfuly");
		
		
		
		
				

	}

}
